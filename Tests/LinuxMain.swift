import XCTest

import BubblzQLTests

var tests = [XCTestCaseEntry]()
tests += BubblzQLTests.allTests()
XCTMain(tests)
