import XCTest
@testable import BubblzQL

final class BubblzQLTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(BubblzQL().text, "Hello, World!")
        let file = AppFile()
        XCTAssertEqual(file.i, 0)

    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
