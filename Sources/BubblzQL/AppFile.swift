//
//  File.swift
//  
//
//  Created by Pan Troian on 03.12.2020.
//

import Foundation

struct AppFile {

    var i = 0
    var path: String?
    
     init(i: Int = 0, path: String? = nil) {
        self.i = i
        self.path = path
    }
    
}
